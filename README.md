# OvalCodeRun
Sphero Android Application
===

__Oval code__ is an _Android_ application that serve REST request routes and JSON response for some _Sphero_ functionality and [Oval](http://sdk.sphero.com/robot-languages/oval-language/) program upload.

For building:

- clone this repository
- download [Sphero Andorid SDK](https://github.com/orbotix/Sphero-Android-SDK)
- put RobotLibrary.jar in _/app/libs_
- put _jniLibs_ folder in _/app/main_
- download [Codemirror](https://codemirror.net/)  and put files in _/app/main/assets/codemirror_
- Import in Android Studio or build directly with gradle

Routes
---

- /api/1.0/code/run"
- /api/1.0/code/upload"
- /api/1.0/editor/run"
- /api/1.0/editor/upload"
- /api/1.0/robot/accelerometer/data"
- /api/1.0/robot/aiming"
- /api/1.0/robot/attitude/data"
- /api/1.0/robot/collision/data"
- /api/1.0/robot/color/$r/$g/$b"
- /api/1.0/robot/connect"
- /api/1.0/robot/disconnect"
- /api/1.0/robot/drive/$h/$v/$[true|false]"
- /api/1.0/robot/gyroscope/data"
- /api/1.0/robot/heading/$h"
- /api/1.0/robot/jump"
- /api/1.0/robot/locator/data"
- /api/1.0/robot/motorBackEMF/data"
- /api/1.0/robot/motors/raw/$[brake|forward|ignore|off|reverse]/$p/$[brake|forward|ignore|off|reverse]/p"
- /api/1.0/robot/odometer"
- /api/1.0/robot/ping"
- /api/1.0/robot/powerState"
- /api/1.0/robot/quaternion/data"
- /api/1.0/robot/rollFor/$h/$v/$d/$[true|false]"
- /api/1.0/robot/rotate/$h"
- /api/1.0/robot/sensors/off"
- /api/1.0/robot/sensors/on"
- /api/1.0/robot/sleep"
- /api/1.0/robot/stabilization/off"
- /api/1.0/robot/stabilization/on"
- /api/1.0/robot/stop"
- /api/1.0/robot/stop/driving"
- "/api/version" 

Screenshots
---

![](images/screenshot-3.png) 
![](images/screenshot-2.png) 
![](images/screenshot-1.png) 
