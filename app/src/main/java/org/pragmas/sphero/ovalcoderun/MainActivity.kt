package org.pragmas.sphero.ovalcoderun

import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

  private val btnStartRestServer by lazy { findViewById(R.id.btnStartRestServer) as Button }
  private val btnRunOvalCode by lazy { findViewById(R.id.btnRunOvalCode) as Button }
  private val btnFindRobot by lazy { findViewById(R.id.btnFindRobot) as Button }
  private val btnResetRobot by lazy { findViewById(R.id.btnResetRobot) as Button }
  private val editHost by lazy { findViewById(R.id.editHost) as TextView }
  private val editPort by lazy { findViewById(R.id.editPort) as EditText }

  private fun initializeUi() {

    editHost.text = Utils.getIPAddress(
      this.getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager
    ).joinToString(System.getProperty("line.separator"))
    editPort.setText(RestServer.port.toString())

    btnStartRestServer.setOnClickListener {
      if (RestServer.started) {
        stopServer()
      } else {
        startServer()
      }
    }

    btnFindRobot.setOnClickListener {
      println(SpheroRunner.runner)
      SpheroRunner.runner.let {
        if (it.isDiscovering) {
          btnFindRobot.text = resources.getText(R.string.findRobot)
          it.stopDiscovery()
        } else {
          btnFindRobot.text = resources.getText(R.string.stopFindRobot)
          it.startDiscovery()
        }
      }
    }

    btnResetRobot.setOnClickListener {
      SpheroRunner.runner.resetOvm()
    }

    btnRunOvalCode.setOnClickListener {
      CodeEditorProxy.runCode()
    }
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_main)
    //Logger.create(findViewById(R.id.logCat) as TextView)
    Logger.initialize(findViewById(R.id.logCat) as TextView)
    SpheroRunner.initialize(this)
    CodeEditorProxy.initialize(findViewById(R.id.editCode) as WebView, SpheroRunner.runner)
    initializeUi()
  }

  private fun stopServer() {
    btnStartRestServer.text = resources.getText(R.string.online)
    RestServer.stop()
    Logger.i(resources.getText(R.string.log_server),
      resources.getText(R.string.offline))
  }

  private fun startServer() {
    btnStartRestServer.text = resources.getText(R.string.offline)
    RestServer.start()
    Logger.i(resources.getText(R.string.log_server),
      resources.getText(R.string.online))
  }

  override fun onStart() {
    super.onStart()
    //SpheroRunner.runner.startDiscovery()
    startServer()
  }

  override fun onStop() {
    SpheroRunner.runner.let {
      it.stopDiscovery()
      it.disconnect()
    }
    stopServer()
    super.onStop()
  }

  override fun onDestroy() {
    SpheroRunner.runner.removeListener()
    super.onDestroy()    
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
    when (requestCode) {
      SpheroRunner.REQUEST_CODE_LOCATION_PERMISSION -> permissions.forEachIndexed { index, s ->
        when (grantResults[index]) {
          PackageManager.PERMISSION_GRANTED -> {
            Logger.i(resources.getText(R.string.log_permission),
              "${resources.getText(R.string.granted)}: $s")
          }
          PackageManager.PERMISSION_DENIED ->
            Logger.i(resources.getText(R.string.log_permission),
              "${resources.getText(R.string.denied)}: $s")
          else ->
            Logger.i(resources.getText(R.string.log_permission),
              "${resources.getText(R.string.error)}: $s")
        }
      }
      else ->
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }


}
