package org.pragmas.sphero.ovalcoderun

import com.orbotix.async.CollisionDetectedAsyncData
import com.orbotix.async.DeviceSensorAsyncMessage
import com.orbotix.command.GetOdometerResponse
import com.orbotix.command.GetPowerStateResponse
import com.orbotix.command.PingResponse
import com.orbotix.common.ResponseListener
import com.orbotix.common.Robot
import com.orbotix.common.internal.AsyncMessage
import com.orbotix.common.internal.DeviceResponse

class SpheroResponseListener : ResponseListener {

  override fun handleResponse(res: DeviceResponse?, r: Robot?) {
    when (res) {
      is GetOdometerResponse -> RobotData.odometer = res
      is GetPowerStateResponse -> RobotData.powerState = res
      is PingResponse -> RobotData.ping = res.code
    }
  }

  override fun handleStringResponse(msg: String?, r: Robot?) {
  }

  override fun handleAsyncMessage(asyncMessage: AsyncMessage?, r: Robot?) {
    when (asyncMessage) {
      is CollisionDetectedAsyncData -> {
        RobotData.collision = asyncMessage
        Logger.i("MSG", "Collision data")
      }
      is DeviceSensorAsyncMessage -> {
        RobotData.sensorsData = asyncMessage
        Logger.i("MSG", "Sensors data")
      }
    }
  }
}
