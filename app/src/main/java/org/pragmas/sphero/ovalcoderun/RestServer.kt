package org.pragmas.sphero.ovalcoderun

import com.google.gson.GsonBuilder
import fi.iki.elonen.NanoHTTPD
import fi.iki.elonen.NanoHTTPD.Response.Status.OK
import java.io.File
import java.io.Serializable

object API {
  val version = "1.0"
  val base = "/api/${version}"

  val paramOvalCode = "ovalCode"
  val paramMethod = "method"
}

object ROUTES {

  val assetsRoot = "/assets"

  val accelerometerData = "${API.base}/robot/accelerometer/data"
  val aiming = "${API.base}/robot/aiming"
  val api_version = "/api/version"
  val attitudeData = "${API.base}/robot/attitude/data"
  val collisionData = "${API.base}/robot/collision/data"
  val color = "${API.base}/robot/color"
  val connect = "${API.base}/robot/connect"
  val disconnect = "${API.base}/robot/disconnect"
  val drive = "${API.base}/robot/drive"
  val formRun = "${API.base}/editor/run"
  val formUpload = "${API.base}/editor/upload"
  val gyroscopeData = "${API.base}/robot/gyroscope/data"
  val heading = "${API.base}/robot/heading"
  val jump = "${API.base}/robot/jump"
  val locatorData = "${API.base}/robot/locator/data"
  val motorBackEMFData = "${API.base}/robot/motorBackEMF/data"
  val odometer = "${API.base}/robot/odometer"
  val ping = "${API.base}/robot/ping"
  val powerState = "${API.base}/robot/powerState"
  val quaternionData = "${API.base}/robot/quaternion/data"
  val rawMotors = "${API.base}/robot/motors/raw"
  val rollFor = "${API.base}/robot/rollFor"
  val rotate = "${API.base}/robot/rotate"
  val runCode = "${API.base}/code/run"
  val sensorsOff = "${API.base}/robot/sensors/off"
  val sensorsOn = "${API.base}/robot/sensors/on"
  val sleep = "${API.base}/robot/sleep"
  val stabilizationOff = "${API.base}/robot/stabilization/off"
  val stabilizationOn = "${API.base}/robot/stabilization/on"
  val stop = "${API.base}/robot/stop"
  val stopDriving = "${API.base}/robot/stop/driving"
  val uploadCode = "${API.base}/code/upload"
}

object STATUS {
  val success = "success"
  //val failure = "failure"
  val robotNotConnected = "robotNotConnected"
  val notFound = "notFound"
  val methodNotAllowed = "methodNotAllowed"
}

object CONTENT_TYPE {
  val json = "application/json"
  val html = "text/html"
  val css = "text/css"
  val js = "application/javascript"
  val jpeg = "image/jpeg"
  val png = "image/png"
  val ico = "image/x-icon"
}

class RestServer(hostname: String?, port: Int) : NanoHTTPD(hostname, port) {

  companion object {

    var host = "0.0.0.0"
    var port = 9999
    var server: RestServer? = null
    var started = false

    fun start(p: Int = this.port) {
      if (server == null)
        server = RestServer(null, p)
      if (!started)
        server?.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false)
      started = true
    }

    fun stop() {
      server?.stop()
      server = null
      started = false
    }
  }

  private val ALLOWED_METHODS = "GET, POST" //, PUT, POST, DELETE, HEAD, OPTIONS"
  private val MAX_AGE = 1800

  private var parameters: Map<String, List<String>> = mutableMapOf()
  private var responseContentType = CONTENT_TYPE.json
  private val jsonProducer = GsonBuilder().setPrettyPrinting().create()
  private var currentSession: IHTTPSession? = null

  init {
    //start(NanoHTTPD.SOCKET_READ_TIMEOUT, false)
  }

  fun addCORSHeaders(response: Response, cors: String): Response {
    response.addHeader("Access-Control-Allow-Origin", cors)
    response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    response.addHeader("Access-Control-Allow-Credentials", "true")
    response.addHeader("Access-Control-Allow-Methods", ALLOWED_METHODS)
    response.addHeader("Access-Control-Max-Age", MAX_AGE.toString())
    return response
  }

  private fun parsePostBody() {
    if (currentSession != null) {
      var body: MutableMap<String, String> = mutableMapOf()
      currentSession!!.parseBody(body)
      parameters = currentSession!!.parameters
    } else parameters = mutableMapOf()
  }

  private fun prepareForm(action: String, code: String = ""): String {
    responseContentType = CONTENT_TYPE.html
    return FileManager.openAsString("assets/form_upload.html")
      .replace("@[action]", action)
      .replace("@[API.paramOvalCode]", API.paramOvalCode)
      .replace("@[code]", code)
  }

  private fun parseParams(run: Boolean = false): String {
    parsePostBody()
    val methodFrom = parameters.get(API.paramMethod)
    val currentCode = parameters.get(API.paramOvalCode)!!.first()
    CodeEditorProxy.setCode(currentCode)
    if (run) SpheroRunner.runner.sendCode(currentCode)
    return if (methodFrom != null && methodFrom.first() == "get")
      if (run) prepareForm(ROUTES.runCode, currentCode)
      else prepareForm(ROUTES.uploadCode, currentCode)
    else
      statusResponse(STATUS.success)
  }

  private fun statusResponse(code: String, map: HashMap<String, Serializable?> = hashMapOf()): String {
    responseContentType = CONTENT_TYPE.json
    val status = hashMapOf<String, Serializable?>("status" to code, "route" to currentSession!!.uri)
    if (!map.isEmpty()) status.putAll(map)
    return jsonProducer.toJson(status)
  }

  private fun getRobotData(data: HashMap<String, Serializable?>) =
    statusResponse(STATUS.success, data)

  private fun sendRobotCommand(f: () -> Unit, response: HashMap<String, Serializable?> = hashMapOf()): String {
    val ret = (SpheroRunner.runner.robot ?: statusResponse(STATUS.robotNotConnected))
      .takeIf { it is String } ?: run {
      f()
      statusResponse(STATUS.success, response)
    }
    return ret as String
  }

  private fun <A> sendRobotCommand(f: (A) -> Unit, a: A, response: HashMap<String, Serializable?> = hashMapOf()): String {
    val ret = (SpheroRunner.runner.robot ?: statusResponse(STATUS.robotNotConnected))
      .takeIf { it is String } ?: run {
      f(a)
      statusResponse(STATUS.success, response)
    }
    return ret as String
  }

  private fun <A, B> sendRobotCommand(f: (A, B) -> Unit, a: A, b: B, response: HashMap<String, Serializable?> = hashMapOf()): String {
    val ret = (SpheroRunner.runner.robot ?: statusResponse(STATUS.robotNotConnected))
      .takeIf { it is String } ?: run {
      f(a, b)
      statusResponse(STATUS.success, response)
    }
    return ret as String
  }

  private fun <A, B, C> sendRobotCommand(f: (A, B, C) -> Unit, a: A, b: B, c: C, response: HashMap<String, Serializable?> = hashMapOf()): String {
    val ret = (SpheroRunner.runner.robot ?: statusResponse(STATUS.robotNotConnected))
      .takeIf { it is String } ?: run {
      f(a, b, c)
      statusResponse(STATUS.success, response)
    }
    return ret as String
  }

  private fun <A, B, C, D> sendRobotCommand(f: (A, B, C, D) -> Unit, a: A, b: B, c: C, d: D, response: HashMap<String, Serializable?> = hashMapOf()): String {
    val ret = (SpheroRunner.runner.robot ?: statusResponse(STATUS.robotNotConnected))
      .takeIf { it is String } ?: run {
      f(a, b, c, d)
      statusResponse(STATUS.success, response)
    }
    return ret as String
  }

  private fun matchRoute(route: String, uri: String) =
    Regex("${route}/(.*)$").matches(uri)

  private fun extractParams(uri: String, base: String): List<*> {
    val r = Regex("${base}/(.*)$")
    val matches = r.matchEntire(uri)
    if (matches != null) {
      val values = ("/" + matches.groups[1]?.value).split("/")
      /*
      val res = if (values != null)
        values.drop(1).map { p ->
          if (p == "true" || p == "false") p.toBoolean()
          else p.toFloatOrNull() ?: p.toIntOrNull() ?: p
        }
      else listOf<Int>()
      */
      val res = values.drop(1).map { p ->
        if (p == "true" || p == "false") p.toBoolean()
        else p.toFloatOrNull() ?: p.toIntOrNull() ?: p
      }
      return res.toList()
    } else return listOf<Int>()
  }

  override fun serve(session: IHTTPSession): Response {
    currentSession = session
    val uri = session.uri
    if (!matchRoute(ROUTES.assetsRoot, uri))
      Logger.i("ROUTE", uri, "em")
    val responseMsg: String = when (session.method.name) {
      "GET" -> when (uri) {
        ROUTES.api_version -> statusResponse(STATUS.success, hashMapOf("version" to API.version))

        ROUTES.formRun -> prepareForm(ROUTES.runCode)
        ROUTES.formUpload -> prepareForm(ROUTES.uploadCode)

        ROUTES.connect -> sendRobotCommand(SpheroRunner.runner::connect)
        ROUTES.disconnect -> sendRobotCommand(SpheroRunner.runner::disconnect)
        ROUTES.aiming -> sendRobotCommand(SpheroRunner.runner::aiming)
        ROUTES.stopDriving -> sendRobotCommand(SpheroRunner.runner::stopDriving)
        ROUTES.stop -> sendRobotCommand(SpheroRunner.runner::stop)
        ROUTES.sleep -> sendRobotCommand(SpheroRunner.runner::sleep)
        ROUTES.stabilizationOn -> sendRobotCommand(SpheroRunner.runner::stabilizationOn)
        ROUTES.stabilizationOff -> sendRobotCommand(SpheroRunner.runner::stabilizationOff)
        ROUTES.sensorsOn -> sendRobotCommand(SpheroRunner.runner::sensorsOn)
        ROUTES.sensorsOff -> sendRobotCommand(SpheroRunner.runner::sensorsOff)
        ROUTES.collisionData -> getRobotData(RobotData.collisionToHashMap())
        ROUTES.locatorData -> getRobotData(RobotData.locatorToHashMap())
        ROUTES.accelerometerData -> getRobotData(RobotData.accelerometerToHashMap())
        ROUTES.gyroscopeData -> getRobotData(RobotData.gyroscopeToHashMap())
        ROUTES.attitudeData -> getRobotData(RobotData.attitudeToHashMap())
        ROUTES.quaternionData -> getRobotData(RobotData.quaternionToHashMap())
        ROUTES.motorBackEMFData -> getRobotData(RobotData.motorBackEMFToHashMap())
        ROUTES.jump -> sendRobotCommand(SpheroRunner.runner::jump)

        ROUTES.ping -> sendRobotCommand(SpheroRunner.runner::ping, RobotData.ping())
        ROUTES.powerState -> sendRobotCommand(SpheroRunner.runner::powerState, RobotData.powerState())
        ROUTES.odometer -> sendRobotCommand(SpheroRunner.runner::odometer, RobotData.odometer())

        else -> {
          if (matchRoute(ROUTES.rotate, uri)) {
            val params = extractParams(uri, ROUTES.rotate)
            sendRobotCommand(SpheroRunner.runner::rotate, params[0] as Float)
          } else if (matchRoute(ROUTES.color, uri)) {
            val params = extractParams(uri, ROUTES.color)
            sendRobotCommand(SpheroRunner.runner::color, params[0] as Float, params[1] as Float, params[2] as Float)
          } else if (matchRoute(ROUTES.drive, uri)) {
            val params = extractParams(uri, ROUTES.drive)
            sendRobotCommand(SpheroRunner.runner::drive, params[0] as Float, params[1] as Float, params[2] as Boolean)
          } else if (matchRoute(ROUTES.rollFor, uri)) {
            val params = extractParams(uri, ROUTES.rollFor)
            sendRobotCommand(SpheroRunner.runner::rollFor, params[0] as Float, params[1] as Float, params[2] as Float, params[3] as Boolean)
          } else if (matchRoute(ROUTES.rawMotors, uri)) {
            val params = extractParams(uri, ROUTES.rawMotors)
            sendRobotCommand(SpheroRunner.runner::rawMotors, params[0] as String, params[1] as Int, params[2] as String, params[3] as Int)
          } else if (matchRoute(ROUTES.heading, uri)) {
            val params = extractParams(uri, ROUTES.heading)
            sendRobotCommand(SpheroRunner.runner::heading, params[0] as Float)
          } else if (matchRoute(ROUTES.assetsRoot, uri)) {
            val path = File(uri.drop(1))
            when (path.extension) {
              "css" -> responseContentType = CONTENT_TYPE.css
              "js" -> responseContentType = CONTENT_TYPE.js
              "jpg" -> responseContentType = CONTENT_TYPE.jpeg
              "png" -> responseContentType = CONTENT_TYPE.png
              "ico" -> responseContentType = CONTENT_TYPE.ico
            }
            FileManager.openAsString(path.toString())
          } else statusResponse(STATUS.notFound)

        }
      }
      "POST" -> when (uri) {
        ROUTES.runCode -> parseParams(true)
        ROUTES.uploadCode -> parseParams(false)
        else -> statusResponse(STATUS.notFound)
      }
      else -> statusResponse(STATUS.methodNotAllowed)
    }
    return addCORSHeaders(newFixedLengthResponse(OK, responseContentType, responseMsg), "*")
  }
}
