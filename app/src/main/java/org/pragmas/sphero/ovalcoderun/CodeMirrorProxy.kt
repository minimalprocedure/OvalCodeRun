package org.pragmas.sphero.ovalcoderun

import android.annotation.SuppressLint
import android.app.Activity
import android.webkit.WebView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

object CodeEditorProxy {

  private lateinit var wv: WebView
  private lateinit var rr: SpheroRunner

  fun initialize(webView: WebView, robotRunner: SpheroRunner) {
    wv = webView
    rr = robotRunner
    wv.let {
      it.settings?.let {
        @SuppressLint("SetJavaScriptEnabled")
        it.javaScriptEnabled = true
        it.blockNetworkLoads = true
        it.setSupportZoom(false)
      }
      (it.context as Activity).runOnUiThread {
        it.loadUrl("file:///android_asset/codemirror.html")
        it.requestFocus()
      }
    }
  }

  fun runCode() {
    wv.let {
      //(it.context as Activity).runOnUiThread {
      launch(UI) {
        it.evaluateJavascript("""
      (function() {
          return codemirror.getValue();
        }
      )();"""
        ) { s -> rr.sendCode(s) }
      }
    }
  }

  fun setCode(code: String) {
    wv.let {
      //(it.context as Activity).runOnUiThread {
      launch(UI) {
        it.evaluateJavascript("""
      (function() {
          codemirror.setValue(`$code`);
          return true;
        }
      )();"""
        ) { _ -> }
      }
    }
  }
}
