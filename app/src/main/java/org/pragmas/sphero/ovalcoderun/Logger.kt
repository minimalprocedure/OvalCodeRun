package org.pragmas.sphero.ovalcoderun

import android.text.Html
import android.text.method.ScrollingMovementMethod
import android.widget.TextView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch

object Logger {

  private var tw: TextView? = null

  fun initialize(logCat: TextView) {
    tw = logCat
    tw?.movementMethod = ScrollingMovementMethod()
    tw?.freezesText = true
  }

  @Suppress("DEPRECATION")
  private fun html(s: String) =
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      Html.fromHtml(s, Html.FROM_HTML_MODE_COMPACT)
    } else {
      Html.fromHtml(s)
    }

  fun i(s: CharSequence, msg: CharSequence, wrap: String = "span") {

    val ctx = if (s.isNullOrBlank()) "" else "<strong>$s:</strong> "
    launch(UI) { tw?.append(html("$ctx<$wrap>$msg</$wrap><br>")) }
    //(logCat.context as Activity).runOnUiThread {
    //  logCat.append(html("$ctx<$wrap>$msg</$wrap><br>"))
    //}
  }

}
