package org.pragmas.sphero.ovalcoderun

import android.net.wifi.WifiManager
import java.net.Inet4Address
import java.net.NetworkInterface

object Utils {

  private fun getLocalIP(wifiManager : WifiManager): String {
    //val wifiManager = appMng.mainActivity!!.getSystemService(AppCompatActivity.WIFI_SERVICE) as WifiManager
    val wifiInfo = wifiManager.connectionInfo
    val ipAddress = wifiInfo.ipAddress
    return listOf(
      ipAddress and 0xff,
      ipAddress shr 8 and 0xff,
      ipAddress shr 16 and 0xff,
      ipAddress shr 24 and 0xff
    ).joinToString(".")
  }

  fun getIPAddress(wifiManager : WifiManager): MutableList<String> {
    val addresses = NetworkInterface.getNetworkInterfaces()
      .toList().fold(mutableListOf<String>()) { a, iface ->
      iface.inetAddresses.toList().forEach { iaddress ->
        val ad = iaddress.takeIf { !it.isLoopbackAddress && it is Inet4Address && !a.contains(it.hostAddress) }
        a += listOfNotNull(ad?.hostAddress)
      }
      a
    }
    addresses += listOfNotNull(getLocalIP(wifiManager).takeIf { !addresses.contains(it) })
    return addresses
  }

}
