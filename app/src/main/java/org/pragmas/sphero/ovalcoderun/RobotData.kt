package org.pragmas.sphero.ovalcoderun

import com.orbotix.async.CollisionDetectedAsyncData
import com.orbotix.async.DeviceSensorAsyncMessage
import com.orbotix.command.GetOdometerResponse
import com.orbotix.command.GetPowerStateResponse
import com.orbotix.common.internal.ResponseCode
import com.orbotix.common.sensor.*
import java.io.Serializable
import java.util.*

object RobotData {

  var collision: CollisionDetectedAsyncData? = null
  var locator: LocatorData? = null
  var accelerometer: AccelerometerData? = null
  var gyroscope: GyroData? = null
  var attitude: AttitudeSensor? = null
  var quaternion: QuaternionSensor? = null
  var motorBackEMF: BackEMFSensor? = null

  var odometer: GetOdometerResponse? = null
  var powerState: GetPowerStateResponse? = null
  var ping: ResponseCode? = null

  private var _sensorData: DeviceSensorAsyncMessage? = null
  var sensorsData: DeviceSensorAsyncMessage?
    get() = _sensorData
    set(value) {
      _sensorData = value
      val data = _sensorData?.asyncData?.get(0)
      accelerometer = data?.accelerometerData
      gyroscope = data?.gyroData
      locator = data?.locatorData
      attitude = data?.attitudeData
      quaternion = data?.quaternion
      motorBackEMF = data?.backEMFData?.emfFiltered
    }

  fun odometer(): HashMap<String, Serializable?> =
    hashMapOf(
      "odometer" to hashMapOf(
        "distance" to (odometer?.distanceInCentimeters ?: 0)
      )
    )

  fun powerState(): HashMap<String, Serializable?> =
    hashMapOf(
      "powerState" to hashMapOf(
        "batteryVoltage" to (powerState?.batteryVoltage ?: 0.0f),
        "numberOfCharges" to (powerState?.numberOfCharges ?: 0),
        "powerState" to hashMapOf(
          "value" to (powerState?.powerState?.value ?: 0)
        ),
        "timeSinceLastCharge" to (powerState?.timeSinceLastCharge ?: 0)
      )
    )

  fun ping(): HashMap<String, Serializable?> =
    hashMapOf(
      "ping" to hashMapOf(
        "code" to (ping?.code ?: 0)
      )
    )

  fun motorBackEMFToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "motorBackEMF" to hashMapOf(
        "left" to (motorBackEMF?.leftMotorValue ?: 0),
        "right" to (motorBackEMF?.rightMotorValue ?: 0)
      )
    )

  fun quaternionToHashMap(): HashMap<String, Serializable?> {
    quaternion?.getQ1()
    return hashMapOf(
      "quaternion" to hashMapOf(
        "q0" to (quaternion?.getQ0() ?: 0.0f),
        "q1" to (quaternion?.getQ1() ?: 0.0f),
        "q2" to (quaternion?.getQ2() ?: 0.0f),
        "q3" to (quaternion?.getQ3() ?: 0.0f),
        "timeStamp" to (quaternion?.timeStamp ?: 0.0f)
      )
    )
  }

  fun attitudeToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "attitude" to hashMapOf(
        "pitch" to (attitude?.pitch ?: 0),
        "roll" to (attitude?.roll ?: 0),
        "yaw" to (attitude?.yaw ?: 0),
        "timeStamp" to (attitude?.timeStamp ?: 0)
      )
    )

  fun gyroscopeToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "gyroscope" to hashMapOf(
        "filtered" to hashMapOf(
          "x" to (gyroscope?.rotationRateFiltered?.x ?: 0),
          "y" to (gyroscope?.rotationRateFiltered?.y ?: 0),
          "z" to (gyroscope?.rotationRateFiltered?.z ?: 0)
        )
      ),
      "timeStamp" to (gyroscope?.timeStamp ?: 0)
    )

  fun accelerometerToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "accelerometer" to hashMapOf(
        "filtered" to hashMapOf(
          "x" to (accelerometer?.filteredAcceleration?.x ?: 0),
          "y" to (accelerometer?.filteredAcceleration?.y ?: 0),
          "z" to (accelerometer?.filteredAcceleration?.z ?: 0)
        ),
        "raw" to hashMapOf(
          "x" to (accelerometer?.rawAcceleration?.x ?: 0),
          "y" to (accelerometer?.rawAcceleration?.y ?: 0),
          "z" to (accelerometer?.rawAcceleration?.z ?: 0)
        )
      ),
      "timeStamp" to (accelerometer?.timeStamp ?: 0)
    )

  fun locatorToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "locator" to hashMapOf(
        "position" to hashMapOf(
          "x" to (locator?.positionX ?: 0.0f),
          "y" to (locator?.positionY ?: 0.0f)
        ),
        "velocity" to hashMapOf(
          "x" to (locator?.velocityX ?: 0.0f),
          "y" to (locator?.velocityY ?: 0.0f)
        )
      ),
      "timeStamp" to (locator?.timeStamp ?: 0)
    )

  fun collisionToHashMap(): HashMap<String, Serializable?> =
    hashMapOf(
      "collision" to hashMapOf(
        "hasImpactYAxis" to (collision?.hasImpactYAxis() ?: false),
        "hasImpactXAxis" to (collision?.hasImpactXAxis() ?: false),
        "impactTimeStamp" to (collision?.impactTimeStamp ?: 0),
        "impactSpeed" to (collision?.impactSpeed ?: 0.0f),

        "impactPower" to hashMapOf(
          "x" to (collision?.impactPower?.x ?: 0),
          "y" to (collision?.impactPower?.y ?: 0)
        ),
        "impactAcceleration" to hashMapOf(
          "x" to (collision?.impactAcceleration?.x ?: 0.0),
          "y" to (collision?.impactAcceleration?.y ?: 0.0),
          "z" to (collision?.impactAcceleration?.z ?: 0.0)
        ),
        "timeStamp" to (collision?.timeStamp ?: 0)
      )
    )
}
