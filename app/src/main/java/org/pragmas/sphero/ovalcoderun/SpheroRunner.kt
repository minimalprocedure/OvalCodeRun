package org.pragmas.sphero.ovalcoderun

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.PermissionChecker
import android.util.Log
import com.orbotix.ConvenienceRobot
import com.orbotix.DualStackDiscoveryAgent
import com.orbotix.command.GetOdometerCommand
import com.orbotix.command.GetPowerStateCommand
import com.orbotix.command.PingCommand
import com.orbotix.command.RawMotorCommand
import com.orbotix.command.RollCommand
import com.orbotix.common.DiscoveryException
import com.orbotix.common.Robot
import com.orbotix.common.RobotChangedStateListener
import com.orbotix.common.sensor.SensorFlag
import com.orbotix.ovalcompiler.OvalControl
import com.orbotix.ovalcompiler.response.async.OvalDeviceBroadcast
import com.orbotix.ovalcompiler.response.async.OvalErrorBroadcast
import com.orbotix.subsystem.SensorControl
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import java.util.*

object RobotMsg {
  val labelRobot = "ROBOT"
  val labelOval = "OVAL"
  val msgReady = "ready"
  val msgNotReady = "not ready"
}

class SpheroRunner(private val activity: Activity) : RobotChangedStateListener, OvalControl.OvalControlListener {

  var robot: ConvenienceRobot? = null
  private var ovalControl: OvalControl? = null

  private val responseListener = SpheroResponseListener()

  companion object {
    val REQUEST_CODE_LOCATION_PERMISSION = 42

    private lateinit var a: MainActivity

    val runner by lazy {
      SpheroRunner(a)
    }

    fun initialize(activity: MainActivity): Companion {
      a = activity
      return this
    }
  }

  init {
    spheroPermissions()
    Logger.i(RobotMsg.labelRobot, RobotMsg.msgNotReady)
    try {
      //startDiscovery()
    } catch (e: RuntimeException) {
      Log.d("EXCEPTION", e.message)
    }
  }

  val isDiscovering: Boolean
    get() = DualStackDiscoveryAgent.getInstance().isDiscovering

  val isConnected: Boolean
    get () = robot?.isConnected ?: false

  fun logNotReady(command: String) =
    Logger.i(RobotMsg.labelRobot, "${RobotMsg.msgNotReady} ($command)")

  /* BEGIN COMMANDS */

  fun odometer() {
    robot?.let {
      Logger.i(RobotMsg.labelRobot, "odometer")
      it.sendCommand(GetOdometerCommand())
    } ?: logNotReady("odometer")
  }

  fun powerState() {
    robot?.let {
      Logger.i(RobotMsg.labelRobot, "powerState")
      it.sendCommand(GetPowerStateCommand())
    } ?: logNotReady("powerState")
  }

  fun ping() {
    robot?.let {
      Logger.i(RobotMsg.labelRobot, "ping")
      it.sendCommand(PingCommand())
    } ?: logNotReady("ping")
  }

  fun sendCode(c: String) {
    ovalControl?.let {
      Logger.i(RobotMsg.labelOval, "send code")
      it.sendOval(c)
    } ?: logNotReady("send code")
  }

  fun heading(h: Float = 0.0f) {
    robot?.let {
      if (h == 0.0f)
        it.setZeroHeading()
      else
        it.drive(h, 0.0f)
      Logger.i(RobotMsg.labelRobot, "heading")
    } ?: logNotReady("heading")

  }

  fun connect() {
    startDiscovery()
    Logger.i(RobotMsg.labelRobot, "connecting...")
  }

  fun disconnect() {
    robot?.let {
      it.removeResponseListener(responseListener)
      it.disconnect()
      robot = null
      Logger.i(RobotMsg.labelRobot, "disconnect")
    } ?: logNotReady("disconnect")

  }

  fun resetOvm() {
    ovalControl?.let {
      it.resetOvm(true)
      Logger.i(RobotMsg.labelRobot, "resetOvm")
    } ?: logNotReady("resetOvm")
  }

  fun aiming() {
    robot?.let {
      it.drive(180.0f, 0.0f)
      it.setZeroHeading()
      Logger.i(RobotMsg.labelRobot, "aiming")
    } ?: logNotReady("aiming")
  }

  private fun rawMotorMode(mode : String) : RawMotorCommand.MotorMode =
    when (mode.toLowerCase()) {
      "brake" -> RawMotorCommand.MotorMode.MOTOR_MODE_BRAKE
      "forward" -> RawMotorCommand.MotorMode.MOTOR_MODE_FORWARD
      "ignore" -> RawMotorCommand.MotorMode.MOTOR_MODE_IGNORE
      "off" -> RawMotorCommand.MotorMode.MOTOR_MODE_OFF
      "reverse" -> RawMotorCommand.MotorMode.MOTOR_MODE_REVERSE
      else -> RawMotorCommand.MotorMode.MOTOR_MODE_OFF
    }

  fun rawMotors(lMode: String, lPower: Int, rMode: String, rPower: Int) {
    robot?.let {
      it.setRawMotors(rawMotorMode(lMode), lPower, rawMotorMode(rMode), rPower)
      Logger.i(RobotMsg.labelRobot, "rawMotors")
    } ?: logNotReady("rawMotors")
  }

  fun stopDriving() {
    robot?.let {
      it.sendCommand(RollCommand(robot?.lastHeading ?: 0.0f, 0.0f, RollCommand.State.STOP))
      Logger.i(RobotMsg.labelRobot, "stopDriving")
    } ?: logNotReady("stopDriving")
  }

  fun jump() {
    robot?.let {
      it.enableStabilization(false)
      it.sendCommand(RollCommand(0.15f, 0f, RollCommand.State.STOP))
      it.sendCommand(RawMotorCommand(
        RawMotorCommand.MotorMode.MOTOR_MODE_FORWARD, 255,
        RawMotorCommand.MotorMode.MOTOR_MODE_FORWARD, 253
        ))
      it.enableStabilization(true)
      it.stop()
      Logger.i(RobotMsg.labelRobot, "jump")
    } ?: logNotReady("jump")
  }

  fun color(rr: Float, gg: Float, bb: Float) {
    robot?.let {
      //it.sendCommand(RGBLEDOutputCommand(rr, gg, bb))
      it.setLed(rr, gg, bb)
      Logger.i(RobotMsg.labelRobot, "color")
    } ?: logNotReady("color")
  }

  fun drive(h: Float, v: Float, rev: Boolean = false) {
    robot?.let {
      robot?.drive(h, v, rev)
      Logger.i(RobotMsg.labelRobot, "drive")
    } ?: logNotReady("drive")
  }

  fun rollFor(h: Float, v: Float, distance: Float, rev: Boolean = false) {
    robot?.let {
      sensorsOn()
      val startX = RobotData.locator?.position?.x ?: 0.0f
      val startY = RobotData.locator?.position?.y ?: 0.0f
      drive(it.lastHeading, v, rev)
      var isDriving = true

      launch {
        while (isDriving) {
          delay(200)
          val nextX = RobotData.locator?.position?.x ?: 0.0f
          val nextY = RobotData.locator?.position?.y ?: 0.0f
          val path = Math.sqrt(
            Math.pow((nextX - startX).toDouble(), 2.0) +
              Math.pow((nextY - startY).toDouble(), 2.0)
          )

          if (path >= distance) {
            isDriving = false
            stopDriving()
            sensorsOff()
          } else if (path >= distance / 3.0) {
            drive(h, v / 3, rev)
          } else if (path >= distance / 2.0) {
            drive(h, v / 2, rev)
          }
        }
      }
      Logger.i(RobotMsg.labelRobot, "roll")
    } ?: logNotReady("roll")
  }

  fun stabilizationOn() {
    robot?.let {
      it.enableStabilization(true)
      Logger.i(RobotMsg.labelRobot, "stabilizationOn")
    } ?: logNotReady("stabilizationOn")
  }

  fun stabilizationOff() {
    robot?.let {
      it.enableStabilization(false)
      Logger.i(RobotMsg.labelRobot, "stabilizationOff")
    } ?: logNotReady("stabilizationOff")
  }

  fun sensorsOn() {
    robot?.let {
      it.enableCollisions(true)
      val sensorFlag =
        SensorFlag.ACCELEROMETER_NORMALIZED.longValue() or
          SensorFlag.GYRO_NORMALIZED.longValue() or
          SensorFlag.ATTITUDE.longValue() or
          SensorFlag.LOCATOR.longValue() or
          SensorFlag.MOTOR_BACKEMF_NORMALIZED.longValue() or
          SensorFlag.QUATERNION.longValue() or
          SensorFlag.VELOCITY.longValue()
      it.enableSensors(sensorFlag, SensorControl.StreamingRate.STREAMING_RATE10)
      Logger.i(RobotMsg.labelRobot, "sensorsOn")
    } ?: logNotReady("sensorsOn")

  }

  fun sensorsOff() {
    robot?.let {
      it.enableCollisions(false)
      it.disableSensors()
      Logger.i(RobotMsg.labelRobot, "sensorsOff")
    } ?: logNotReady("sensorsOff")

  }

  fun stop() {
    robot?.let {
      it.stop()
      Logger.i(RobotMsg.labelRobot, "stop")
    } ?: logNotReady("stop")
  }

  fun rotate(h: Float) {
    robot?.let {
      it.rotate(h)
      Logger.i(RobotMsg.labelRobot, "rotate")
    } ?: logNotReady("rotate")
  }

  fun sleep() {
    robot?.let {
      it.sleep()
      Logger.i(RobotMsg.labelRobot, "sleep")
    } ?: logNotReady("sleep")
  }

  /* END COMMANDS */

  private fun spheroPermissions() {
    DualStackDiscoveryAgent.getInstance().addRobotStateListener(this)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      val hasLocationPermission = PermissionChecker.checkSelfPermission(
        activity.baseContext,
        Manifest.permission.ACCESS_COARSE_LOCATION)
      if (hasLocationPermission != PackageManager.PERMISSION_GRANTED) {
        //Logger.i(activity.resources.getText(R.string.log_permission), "Location permission has not already been granted")
        val permissions: MutableList<String> = mutableListOf()
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(activity, permissions.toTypedArray(), REQUEST_CODE_LOCATION_PERMISSION)
      } else {
        Logger.i(RobotMsg.labelRobot, "Location permission already granted")
      }
    }
  }

  fun startDiscovery() {
    if (!isDiscovering) {
      try {
        Logger.i(RobotMsg.labelRobot, "Start Discovery...")
        DualStackDiscoveryAgent.getInstance().startDiscovery(activity)
      } catch (e: DiscoveryException) {
        Logger.i(RobotMsg.labelRobot, "DiscoveryException: ${e.message}")
      }
    }
  }

  fun stopDiscovery() {
    if (isDiscovering) {
      DualStackDiscoveryAgent.getInstance().stopDiscovery()
    }
    Logger.i(RobotMsg.labelRobot, "Stop Discovery.")
  }

  fun removeListener() {
    DualStackDiscoveryAgent.getInstance().addRobotStateListener(null)
  }

  override fun handleRobotChangedState(r: Robot, type: RobotChangedStateListener.RobotChangedStateNotificationType) {
    when (type) {
      RobotChangedStateListener.RobotChangedStateNotificationType.Online -> {
        robot = ConvenienceRobot(r)
        robot?.let {
          it.addResponseListener(responseListener)
          Logger.i(RobotMsg.labelRobot, RobotMsg.msgReady)
        } ?: Logger.i(RobotMsg.labelRobot, RobotMsg.msgNotReady)

        ovalControl = OvalControl(r, this)
        ovalControl?.let {
          Logger.i(RobotMsg.labelOval, RobotMsg.msgReady)
          it.resetOvm(true)
        } ?: Logger.i(RobotMsg.labelOval, RobotMsg.msgNotReady)

      }
      else -> {
      }
    }
  }

  override fun onOvmReset(control: OvalControl) {
    ovalControl?.let {
      Logger.i(RobotMsg.labelOval, "Reset")
    } ?: Logger.i(RobotMsg.labelOval, RobotMsg.msgNotReady)
  }

  override fun onOvalQueueEmptied(p0: OvalControl?) {
  }

  override fun onOvalNotificationReceived(control: OvalControl, notification: OvalDeviceBroadcast) {
    Logger.i(RobotMsg.labelOval, "Received oval notification: " + Arrays.toString(notification.ints))
  }

  override fun onOvmRuntimeErrorReceived(control: OvalControl, notification: OvalErrorBroadcast) {
    Logger.i(RobotMsg.labelOval, "Received oval error: " + notification.errorCode)
  }

  override fun onProgramFailedToSend(p0: OvalControl?, p1: String?) {
  }

  override fun onProgramSentSuccessfully(p0: OvalControl?) {
  }

}

